var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var async = require('async');

exports.acceptBooking = function (req, res) {

	var accessToken = req.body.access_token;
	var bookingID = req.body.booking_id;

	var manValues = [accessToken, bookingID];

	async.waterfall ([

		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			extraDataNeeded = [];
			commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
		}], function (err, result) {

			if (err) {
				sendResponse.somethingWentWrongError(res);
			} else {

				var driverID = result[0].driver_id;

				var sql = 'UPDATE `booking` SET `driver_id`=?, `status`=? WHERE `booking_id`=? AND `status`=?';
				dbConnection.Query(res, sql, [driverID, 1, bookingID, 0], function (response) {

					if (response.affectedRows == 0) {
						var msg = "Booking already accepted";
						sendResponse.showErrorMessage(msg, res);
					} else {
						var msg = "Successfully accepted booking";
						var data = {};
						sendResponse.sendSuccessData(msg, data, res);
					}
				});
			}
		}
	);
};

exports.startService = function (req, res) {

	var accessToken = req.body.access_token;
	var bookingID = req.body.booking_id;

	var manValues = [accessToken, bookingID];

	async.waterfall ([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			extraDataNeeded = [];
			commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
		}], function (err, result) {

			if (err) {
				sendResponse.somethingWentWrongError(res);
			} else {
				var driverID = result[0].driver_id;
				var now = new Date();

				var sql = 'UPDATE `booking` SET `status`=?, `start_time`=? WHERE `booking_id`=? AND `driver_id`=?';
				dbConnection.Query(res, sql, [2, now, bookingID, driverID], function (response) {
					var msg = "Service started";
					var data = {};
					sendResponse.sendSuccessData(msg, data, res);
				});
			}
		});
};

exports.endService = function (req, res) {

	var accessToken = req.body.access_token;
	var bookingID = req.body.booking_id;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;

	var manValues = [accessToken, bookingID, latitude, longitude];

	async.waterfall ([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			extraDataNeeded = [];
			commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
		}], function (err, result) {

			if (err) {
				sendResponse.somethingWentWrongError(res);
			} else {

				var driverID = result[0].driver_id;
				var now = new Date();
				var sql = 'UPDATE `booking`, `drivers` SET booking.`status`=?, booking.`end_time`=?, booking.`drop_latitude`=?, booking.`drop_longitude`=?, drivers.`latitude`=?, drivers.`longitude`=? WHERE booking.`booking_id`=? AND drivers.`driver_id`=?';
				dbConnection.Query(res, sql, [3, now, latitude, longitude, latitude, longitude, bookingID, driverID], function (response) {

					var msg = "Service ended";
					var data = {};
					sendResponse.sendSuccessData(msg, data, res);

				});
			}
		});
};