var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');
var AWSSettings = require('../config/AWSSettings');

exports.getProfileData = function (req, res) {

  var accessToken = req.body.access_token;
  var manValues = [accessToken];

  async.waterfall([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function (callback) {
      var extraDataNeeded = ['email', 'mobile', 'image', 'first_name', 'last_name'];
      commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
    }], function (err, result) {

      if (err) {
        sendResponse.somethingWentWrongError(res);
      } else {
        var msg = "";
        sendResponse.sendSuccessData(msg, result, res);
      }
    }
  );
};

exports.editProfileData = function (req, res) {

  var accessToken = req.body.access_token;
  var email = req.body.email;
  var firstName = req.body.first_name;
  var lastName = req.body.last_name;
  var mobile = req.body.mobile;
  var image = req.files.profile_pic;

  var manValues = [accessToken, email, mobile, firstName, lastName];

  async.waterfall ([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    },
    function (callback) {
      var extraDataNeeded = ['image'];
      commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
    }, function (result, callback) {

      if ((image) && (image.name)) {
        commonfunction.uploadImage(image, AWSSettings.folder, function (imageUrl) {
        callback(result, imageUrl);
        });
      } else {
        callback(result, result[0].image);
      }

    }], function (result, imageName) {

      var driverID = result[0].driver_id;

      var sql = 'SELECT `driver_id` FROM `drivers` WHERE `email`=? LIMIT 1';
      dbConnection.Query(res, sql, [email], function (response1) {

        if (response1.length > 1) {
          var error = 'Email already in use.';
          sendResponse.sendErrorMessage(error, res);
        } else {
          var sql = 'UPDATE `drivers` SET `email`=?,`image`=?,`mobile`=?,`first_name`=?,`last_name`=? WHERE `driver_id`=? limit 1';
          dbConnection.Query(res, sql, [email, imageName, mobile, firstName, lastName, driverID], function (response) {

            var msg = "Profile updated successfully.";
            var data = {"image":imageName}
            sendResponse.sendSuccessData(msg, data, res);
          });
        }
      });
    }
  );
};

exports.changePassword = function (req, res) {

  var accessToken = req.body.access_token;
  var oldPass = req.body.old_password;
  var newPass = req.body.new_password;

  var manValues = [accessToken, oldPass, newPass];

  async.waterfall([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function (callback) {
      var extraDataNeeded = ['password'];
      commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
    }], function (err, result) {

      oldPass = md5(oldPass);
      if (oldPass !== result[0].password) {
        var error = 'Incorrect old password';
        sendResponse.sendErrorMessage(error, res);
      } else {
        newPass = md5(newPass);
        driverID = result[0].driver_id;

        var sql = 'UPDATE `drivers` SET `password`=? WHERE `driver_id`=? LIMIT 1';
        dbConnection.Query(res, sql, [newPass, driverID], function (response) {
          var msg = "Password changed successfully";
          var data = {};
          sendResponse.sendSuccessData(data, res);
        });
      }
    });
};