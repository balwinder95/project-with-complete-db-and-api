var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var generatePassword = require('password-generator');
var async = require('async');
var port = require('../config/localDevelopment').PORT;
var AWSSettings = require('../config/AWSSettings');


exports.registerFromEmail = function (req, res) {
	var firstName = req.body.first_name;
  var lastName = req.body.last_name;
  var email = req.body.email;
  var mobile = req.body.mobile;
  var password = req.body.password;
  var latitude = req.body.latitude;
  var longitude = req.body.longitude;
  var deviceType = req.body.device_type;
  var deviceToken = req.body.device_token;
  var appVersion = req.body.app_version;
  var image = req.files.profile_pic;

  var manValues = [firstName, lastName, email, mobile, password, latitude, longitude, deviceType, deviceToken, appVersion];

  async.waterfall([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    },
    function (callback) {
      checkEmailAvailability(res, email, callback);
    }, function (callback) {
      commonfunction.uploadImage(image, AWSSettings.folder, callback);
    }], function (imageLink) {

      var loginTime = new Date();
      var accessToken = commonfunction.encrypt(email + loginTime);
      var encryptPassword = md5(password);
      var sql = 'INSERT INTO `drivers`(`access_token`, `first_name`, `last_name`, `email`, `mobile`, `image`, `password`, `latitude`, `longitude`, `device_type`, `device_token`, `updated_at`,`app_version`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)';
      dbConnection.Query(res, sql, [accessToken, firstName, lastName, email, mobile, imageLink, encryptPassword, latitude, longitude, deviceType, deviceToken, loginTime, appVersion], function (response) {

        var msg = "You have been registered. Please wait for admin to approve your request";
        var data = {};
        sendResponse.sendSuccessData(msg, data, res);

      });
  });
};


exports.emailLogin = function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  var latitude = req.body.latitude;
  var longitude = req.body.longitude;
  var deviceType = req.body.device_type;
  var deviceToken = req.body.device_token;
  var appVersion = req.body.app_version;

  var manValues = [email, password, latitude, longitude, deviceType, appVersion];

  async.waterfall([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function (callback) {
      checkEmailAndPassword(res, email, password, callback);
    }], function (err, response) {

    var driverID = response[0].driver_id;
    var loginTime = new Date();
    var accessToken = commonfunction.encrypt(email + loginTime);

    var msg = "";
    var data = {access_token: accessToken};
    updateUserParams(res, accessToken, deviceToken, deviceType, loginTime, appVersion, driverID, latitude, longitude);
    sendResponse.sendSuccessData(msg, data, res);
    });
};

exports.accessTokenLogin = function (req, res) {
  var accessToken = req.body.access_token;
  var latitude = req.body.latitude;
  var longitude = req.body.longitude;
  var deviceType = req.body.device_type;
  var deviceToken = req.body.device_token;
  var appVersion = req.body.app_version;
  var latitude = req.body.latitude;
  var longitude = req.body.longitude;

  var manValues = [accessToken, deviceType, appVersion];

  async.waterfall([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function (callback) {
      var extraDataNeeded = ['first_name', 'last_name', 'mobile', 'image', 'email'];
      commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
    }], function (err, response) {

    var driverID = response[0].driver_id;
    var loginTime = new Date();
    accessToken = commonfunction.encrypt(response[0].email + loginTime);
    updateUserParams(res, accessToken, deviceToken, deviceType, loginTime, appVersion, driverID, latitude, longitude);

    var msg = "";
    var data = {access_token: accessToken, first_name: response[0].first_name, last_name: response[0].last_name, mobile: response[0].mobile, image: response[0].image, driver_id: driverID, email: response[0].email};
    sendResponse.sendSuccessData(msg, data, res);
  }
  );
};

exports.userLogout = function (req, res) {
  var accessToken = req.body.access_token;
  var manValues = [accessToken];

  async.waterfall([
    function(callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function(callback) {
      var extraDataNeeded = [];
      commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
    }], function (err, result) {

      var driverID = result[0].driver_id;

      var sql = 'UPDATE `drivers` SET `device_type`=?,`device_token`=? WHERE `driver_id`=? limit 1';
      dbConnection.Query(res, sql, [0, 0, driverID], function (response) {

        var msg = "Logged out successfully.";
        var data = {};
        sendResponse.sendSuccessData(msg, data, res);
    });
  });
};

exports.forgotPassword = function (req, res) {

var email = req.body.email;
var manValues = [email];

async.waterfall([
  function (callback) {
    commonfunction.checkBlankWithCallback(res, manValues, callback);
  }, function (callback) {
    getUserIdFromEmail(res, email, callback);
  }], function (err, driverID) {

    var token = generatePassword(20, false);
    var date = new Date();
    token = md5(date + token);

    var sql = 'UPDATE `drivers` SET `forgot_password_token`=? WHERE `driver_id`=? LIMIT 1';
    dbConnection.Query(res, sql, [token, driverID], function (response) {

      forgotPasswordRequest(email, token, function (result) {

        if (result == 0) {
          sendResponse.somethingWentWrongError(res);
        } else {
          var msg = "Check your email to change your password.";
          var data = {};
          sendResponse.sendSuccessData(msg, data, res);
        }
      });
    });

  });
};

exports.resetPassword = function (req, res) {

  var token = req.body.token;
  var newPass = req.body.new_password;

  var manValues = [token, newPass];

  async.waterfall([
    function (callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function (callback) {
      checkToken(res, token, callback);
    }], function (err, driverID) {

      var newPassEncrypt = md5(newPass);
      var sql = 'UPDATE `drivers` SET `password`=?, `forgot_password_token`=? WHERE `driver_id`=?';
      dbConnection.Query(res, sql, [newPassEncrypt, '', driverID], function (response) {
        var msg = "Password changed succesfully";
        var data = {};
        sendResponse.sendSuccessData(msg, data, res);
      });
    });
};

function checkEmailAndPassword (res, email, password, callback) {

  var encryptPassword = md5(password);

  var sql = 'SELECT `driver_id`, `access_token`,`first_name`,`last_name`,`mobile` FROM `drivers` WHERE `email`=? and `password`=? LIMIT 1';
  dbConnection.Query(res, sql, [email, encryptPassword], function (response) {

    if (!response.length) {
        var error = 'The email or password you entered is incorrect.';
        sendResponse.sendErrorMessage(error, res);
    } else {
        callback(null, response);
    }
  });
}

function updateUserParams (res, accessToken, deviceToken, deviceType, loginTime, appVersion, driverID, latitude, longitude) {

  var sql = 'UPDATE `drivers` SET `access_token`=?, `device_type`=?,`device_token`=?,`app_version`=?,`updated_at`=?, `latitude`=?, `longitude`=? WHERE `driver_id`=? limit 1';
  dbConnection.Query(res, sql, [accessToken, deviceType, deviceToken, appVersion, loginTime, latitude, longitude, driverID], function (result) {
  });
}

function checkEmailAvailability (res, email, callback) {

  var sql = 'SELECT `driver_id` FROM `drivers` WHERE `email`=? limit 1';
  dbConnection.Query(res, sql, [email], function (response) {

    if (response.length > 0) {
      var error = 'Email already exists.';
      sendResponse.sendErrorMessage(error, res);
    } else {
      callback();
    }
  });
}

function getUserIdFromEmail (res, email, callback) {

  var sql = 'SELECT `driver_id` FROM `drivers` WHERE `email`=? LIMIT 1';
  dbConnection.Query(res, sql, [email], function (response) {

    if (response.length == 0) {
      var error = 'Email not registered';
      sendResponse.sendErrorMessage(error, res);
    } else {
      var driverID = response[0].driver_id;
      callback(null, driverID);
    }
  });
}

function forgotPasswordRequest (email, token, callback) {

  var to = email;
  var sub = "Please reset your password";
  var msg = "Hi, <br><br>";
  msg += "http://localhost:" + port + "/reset_password?token=" + token;

  commonfunction.sendEmail(to, msg, sub, function(result) {
      return callback(result);
  });
}

function checkToken (res, token, callback) {

  var sql = 'SELECT `driver_id` FROM `drivers` WHERE `forgot_password_token`=? LIMIT 1';
  dbConnection.Query(res, sql, [token], function (response) {

    if (response.length == 0) {
      var error = 'Invalid reset password token';
      sendResponse.sendErrorMessage(error, res);
    } else {
      var driverID = response[0].driver_id;
      callback(null, driverID);
    }
  });
}