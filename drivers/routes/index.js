var express = require('express');
var router = express.Router();
var driverLogin = require('./driverLogin');
var driverProfile = require('./driverProfile');
var booking = require('./booking');

/* GET test page. */
router.get('/test', function (req, res) {
  res.render('test');
});

router.get('/reset_password', function (req, res) {
	res.render('reset_password');
});

router.post('/registerFromEmail', driverLogin.registerFromEmail);
router.post('/emailLogin', driverLogin.emailLogin);
router.post('/accessTokenLogin', driverLogin.accessTokenLogin);
router.post('/userLogout', driverLogin.userLogout);
router.post('/forgotPassword', driverLogin.forgotPassword);
router.post('/resetPassword', driverLogin.resetPassword);

router.post('/getProfileData', driverProfile.getProfileData);
router.post('/editProfileData', driverProfile.editProfileData);
router.post('/changePassword', driverProfile.changePassword);

router.post('/acceptBooking', booking.acceptBooking);
router.post('/startService', booking.startService);
router.post('/endService', booking.endService);

module.exports = router;