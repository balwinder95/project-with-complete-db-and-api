var sendResponse = require('./sendResponse');
var AWSSettings = require('../config/AWSSettings');
var fs = require('fs');
var AWS = require('aws-sdk');
var randomstring = require('randomstring');
var nodemailer = require('nodemailer');
var emailSettings = require('../config/localDevelopment').emailSettings;

exports.checkBlank = function (arr) {
  return (checkBlank(arr));
};

function checkBlank(arr) {
  var arrlength = arr.length;
  for (var i = 0; i < arrlength; i++) {
    if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
      console.log(arr[i-1]);
      return 1;
    }
  }
  return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

  var checkBlankData = checkBlank(manValues);

  if (checkBlankData) {
    console.log("111");
    sendResponse.parameterMissingError(res);
  }
  else {
    callback(null);
  }
}

exports.encrypt = function (text) {

  var crypto = require('crypto');
  var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
  var crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

exports.authenticateAccessTokenAndReturnExtraData = function (res, accessToken, arr, callback) {

  var sql = 'SELECT `driver_id`';
  arr.forEach(function(entry) {
    sql += ',' + entry;
  });
  sql += ' FROM `drivers`';
  sql += ' WHERE `access_token`=? LIMIT 1';
  dbConnection.Query(res, sql, [accessToken], function (result) {

    if (result.length > 0) {
      return callback(null, result);
    } else {
      sendResponse.invalidAccessTokenError(res);
    }
  });
}

exports.uploadImage = function (file, folder, callback) {

  var fileUrl = 'https://' + AWSSettings.awsBucket + '.s3.amazonaws.com/' + folder + '/';
  if ((file) && (file.name)) {
    uploadToS3Bucket(file, folder, function (fileName) {
      return callback(fileUrl + fileName);
    });
  } else {
    return callback (fileUrl + 'logo.jpg');
  }
};

function uploadToS3Bucket (file, folder, callback) {

  var fileName = file.name;
  var filePath = file.path;
  var fileType = file.type;
  fs.readFile (filePath, function (error, fileBuffer) {

    if (error) {
      callback('logo.jpg');
    } else {
      var str = randomstring.generate(7);
      fileName = 'balwinder-' + str + '-' + file.name;
      fileName = fileName.split(' ').join('-');

      AWS.config.update({accessKeyId: AWSSettings.awsAccessKey, secretAccessKey: AWSSettings.awsSecretKey});
      var s3bucket = new AWS.S3();
      var params = {Bucket: AWSSettings.awsBucket, Key: folder + '/' + fileName, Body: fileBuffer, ACL: 'public-read', ContentType: fileType};
      s3bucket.putObject(params, function (err, data) {
        if (err) {
          return callback('logo.jpg');
        }
        else {
          return callback(fileName);
        }
      });
    }
  });
}

exports.sendEmail = function(receiver, message, subject, callback) {

  var smtpTransport = nodemailer.createTransport('SMTP', {
    service: 'Gmail',
    auth: {
      user: emailSettings.email,
      pass: emailSettings.password
    }
  });

  var mailOptions = {
    from: 'XYZ <' + emailSettings.email + '>',
    to: receiver,
    subject: subject,
    html: message
  }

  smtpTransport.sendMail(mailOptions, function (error, response) {
  if (error) {
    return callback(0);
  } else {
    return callback(1);
  }
  });
}
