process.env.NODE_ENV = 'localDevelopment';

var emailSettings = require('./config/localDevelopment').emailSettings;
var port = require('./config/localDevelopment').PORT;
var path = require('path');
var routes = require('./routes/index');
var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var app = express();
dbConnection = require('./routes/dbConnection');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/test', routes);
app.get('/reset_password', routes);
app.use('/registerFromEmail', multipartMiddleware);
app.post('/registerFromEmail', routes);
app.post('/emailLogin', routes);
app.post('/accessTokenLogin', routes);
app.post('/userLogout', routes);
app.post('/forgotPassword', routes);
app.post('/resetPassword', routes);

app.post('/getProfileData', routes);
app.use('/editProfileData', multipartMiddleware);
app.post('/editProfileData', routes);
app.post('/changePassword', routes);

app.post('/acceptBooking', routes);
app.post('/startService', routes);
app.post('/endService', routes);

// catch 404 and forward to error handler
app.use(function (req, res) {
    var err = new Error('Not Found');
    err.status = 404;
});

app.listen(port, function(){
    console.log('Working on port ' + port);
});