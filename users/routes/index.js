var express = require('express');
var router = express.Router();
var userLogin = require('./userLogin');
var userProfile = require('./userProfile');
var booking = require('./booking');

/* GET test page. */
router.get('/test', function (req, res) {
  res.render('test');
});

router.get('/reset_password', function (req, res) {
	res.render('reset_password');
});

router.post('/registerFromEmail', userLogin.registerFromEmail);
router.post('/emailLogin', userLogin.emailLogin);
router.post('/accessTokenLogin', userLogin.accessTokenLogin);
router.post('/userLogout', userLogin.userLogout);
router.post('/forgotPassword', userLogin.forgotPassword);
router.post('/resetPassword', userLogin.resetPassword);

router.post('/getProfileData', userProfile.getProfileData);
router.post('/editProfileData', userProfile.editProfileData);
router.post('/changePassword', userProfile.changePassword);

router.post('/getDrivers', booking.getDrivers);
router.post('/booking', booking.booking);

module.exports = router;