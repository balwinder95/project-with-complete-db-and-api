var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var async = require('async');
var geolib = require('geolib');

exports.getDrivers = function (req, res) {

	var accessToken = req.body.access_token;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var radius = req.body.radius;

	var manValues = [accessToken, latitude, longitude, radius];

	async.waterfall ([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			var extraDataNeeded = [];
			commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
		}], function (err, result) {

			if (err) {
				sendResponse.somethingWentWrongError(res);
			} else {

				findDrivers(res, latitude, longitude, radius, function (err, drivers) {
					console.log(drivers);
					var message = "";
					sendResponse.sendSuccessData(message, drivers, res);
				});
			}
		});
};


exports.booking  =function (req, res) {

	var accessToken = req.body.access_token;
	var bookingTime = req.body.booking_time;
	var address = req.body.address;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var radius = req.body.radius;

	var manValues = [accessToken, address, latitude, longitude, radius];

	async.waterfall ([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			var extraDataNeeded = [];
			commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);
		}], function (err, result) {

			if (err) {
				sendResponse.somethingWentWrongError(res);
			} else {
				var userID = result[0].user_id;
				var sql = 'INSERT INTO `booking` (`user_id`, `booking_time`, `pick_address`, `pick_latitude`, `pick_longitude`) VALUES(?,?,?,?,?)';
				dbConnection.Query(res, sql, [userID, bookingTime, address, latitude, longitude], function (response) {

					var bookingID = response.insertId;
					var message = "Booking made successfully. Wait for a confirmation from driver";
					var data = {"booking_id": bookingID};
					sendResponse.sendSuccessData(message, data, res);
				});

				findDrivers(res, latitude, longitude, radius, function (err, drivers) {

					if (err) {
						sendResponse.somethingWentWrongError(res);
					}
				});
			}
		});
};


function findDrivers (res, userLatitude, userLongitude, radius, callback) {

	var sql = 'SELECT `driver_id`, `latitude`, `longitude` FROM `drivers` WHERE `driver_verified`=? AND `device_type`!=?';
	dbConnection.Query(res, sql, [1, 0], function (response) {

		var arr = [];
		async.each(response, function (item, callback1) {
			if ( geolib.isPointInCircle ({latitude: item.latitude, longitude: item.longitude}, {latitude: userLatitude, longitude: userLongitude}, radius)) {

				var distance = geolib.getDistance ({latitude: item.latitude, longitude: item.longitude}, {latitude: userLatitude, longitude: userLongitude});

				arr.push({"driver_id": item.driver_id, "distance": distance});
			}
			callback1();
		}, function (err) {

			if (err) {
				sendResponse.somethingWentWrongError(res);
			} else {
				commonfunction.sortByKeyAsc(arr, "distance");
				callback (null, arr);
			}
		});

	});
}